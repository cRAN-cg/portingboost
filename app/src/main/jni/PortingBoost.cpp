#include <jni.h>
#include <cstdio>
#include "boost_1_51_0/boost/array.hpp"
#include <android/log.h>


#ifdef __cplusplus
extern "C" {
#endif
JNIEXPORT jint JNICALL
Java_com_example_cran_1cg_portingboost_MainActivity_naMinusSubject(JNIEnv *pEnv,
                                                                     jobject instance,  jint pInputnumber ) {

    boost::array<short, 5> aShort = {0, 1, 2} ; // Initialization into {0, 1, 2, 0, 0}

    aShort[0]  = 42 ;
    aShort[12] = 42 ; // Fast Access : debug : exception, release : memory corruption

    aShort.at(0)  = 42 ; // Checked Access : Ok
    aShort.at(12) = 42 ; // Checked Access : exception !

    // Ok: 5 elements
    size_t iArraySize = aShort.size() ;
    return (static_cast<int>(iArraySize)- pInputnumber);
    __android_log_print(ANDROID_LOG_INFO,"native", "%d size",iArraySize);
}
#ifdef __cplusplus
}
#endif
