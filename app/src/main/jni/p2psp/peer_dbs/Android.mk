LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := peer_dbs
LOCAL_CPP_EXTENSION := .cxx .cpp .cc
LOCAL_SRC_FILES := peer_dbs.cc
LOCAL_STATIC_LIBRARIES := boost_regex boost_wserialization boost_wave boost_unit_test_framework boost_timer boost_thread boost_test_exec_monitor boost_system boost_signals boost_serialization boost_random boost_program_options boost_prg_exec_monitor boost_math_tr1l boost_math_tr1f boost_math_tr1 boost_math_c99l boost_math_c99f boost_math_c99 boost_iostreams boost_graph boost_filesystem boost_exception boost_date_time boost_context boost_chrono
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)
include $(BUILD_SHARED_LIBRARY)
