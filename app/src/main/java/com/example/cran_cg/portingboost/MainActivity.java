package com.example.cran_cg.portingboost;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv = new TextView(this);
        tv.setText(naMinusSubject(3));
        setContentView(tv);
    }


    static{
        System.loadLibrary("PortingBoost");
        System.loadLibrary("P2PSP-Core");
    }

    private native int naMinusSubject(int x);

}
